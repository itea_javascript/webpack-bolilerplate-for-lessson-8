const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = function( env ){
  return {
    module: {
        rules: [
          {
            test: /\.less$/,
            exclude: /(node_modules)/,
            use: ExtractTextPlugin.extract({
              publicPath: '../',
              fallback: 'style-loader',
              use: [
                'css-loader',
                'less-loader'
              ]
            })
          },
          {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
              publicPath: '../',
              fallback: 'style-loader',
              use: [
                'css-loader'
              ]
            })
          }
        ]
    }, // Module
    plugins: [
      new ExtractTextPlugin('./css/[name].css')
    ]
  };
};
