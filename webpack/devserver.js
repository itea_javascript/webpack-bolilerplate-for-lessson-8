module.exports = function( env ){
  return {
    devServer: {
      contentBase: './public',
      port: 3366
    }
  };
};
