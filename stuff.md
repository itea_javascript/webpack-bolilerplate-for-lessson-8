Применить вебпак по конфигу - 1. source file 2. output file
webpack ./source/index.js ./public/bundle.js


Command keys in npm
  -p production mode - minified file
  Difference:
    unminified: 5371 b
    minified: 1277 b

  -w watch files changes
  Auto-reload when files changes


  Modules (Node):

    path -> Одинаковая работа с url на разных системах
    node-static -> хостинг статики на сервере
      npm i -D node-static
      https://www.npmjs.com/package/node-static

  Loaders:
    https://webpack.js.org/loaders/

      style-loader -> Загрузка стилей в наш DOM
        https://webpack.js.org/loaders/style-loader/
        npm install -D style-loader

      css-loader -> compile css into common-js (node)
        https://webpack.js.org/loaders/css-loader/
        npm install -D css-loader

      less-loader -> compile less in css
        https://webpack.js.org/loaders/less-loader/
        npm install --save-dev less-loader less


  Plugins (Webpack):

    webpack-dev-server
      https://webpack.js.org/configuration/dev-server/#src/components/Sidebar/Sidebar.jsx

    webpack-merge - Склейка массивов и обьектов.
      npm install webpack-merge -D

    extract-text-webpack-plugin
      npm install extract-text-webpack-plugin -D
      https://github.com/webpack-contrib/extract-text-webpack-plugin

    html-webpack-plugin
      npm install html-webpack-plugin --save-dev
      https://www.npmjs.com/package/html-webpack-plugin

  Environment (Окружение)
    -dev
    -prod
