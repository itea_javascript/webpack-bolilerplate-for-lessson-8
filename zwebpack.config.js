const path = require('path');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const devserver = require('./webpack/devserver');
const extractCss = require('./webpack/css.extract.js');
const webpack = require('webpack');


const common = {
  entry:  {
    darkSide:  './source/darkSide.js',
    lightSide: './source/lightSide.js'
  },
  output: {
    filename: 'js/[name].js',
    path: path.resolve(__dirname, 'public')
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        options: {
          presets: ['env']
        }
      },
      {
        test: /\.ejs/,
        loader: 'ejs-loader'
      },
      {
        test: /\.html/,
        loader: 'html-loader'
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
       filename: 'darkSide.html',
       title: 'Dark side of the moon',
       chunk: ['darkSide', 'common'],
       template: './source/templates/dark.html'
     }),
     new HtmlWebpackPlugin({
        filename: 'lightSide.html',
        files: {
          "css": ["/css/lightSide.css"]
        },
        title: 'Light side of the moon',
        chunk: ['lightSide', 'common'],
        template: './source/templates/light.html'
      }),
      new webpack.optimize.CommonsChunkPlugin({
        name: 'common'
      })
  ]
};

module.exports = function( env ){
  if( env === "prod"){
    return merge([
      extractCss(),
      common
    ]);
  }
  if( env === "dev"){
    return merge([
      common,
      extractCss(),
      devserver()
    ]);
  }
};
